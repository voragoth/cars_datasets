/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.utils;

import cl.ubb.recsys.datasets.commons.DatasetReaderIterator;
import cl.ubb.recsys.datasets.commons.StringSeparator;
import cl.ubb.recsys.datasets.foursquare.umn.beans.CheckinsDatasetBean;
import cl.ubb.recsys.datasets.foursquare.umn.beans.RatingsDatasetBean;
import cl.ubb.recsys.datasets.foursquare.umn.beans.VenuesDatasetBean;
import java.io.File;

/**
 *
 * @author Manuel J. Vasquez
 */
public final class DatasetReaderIteratorFactory {
    private DatasetReaderIteratorFactory(){}
    public static StringSeparator STRING_SEPARATOR = StringSeparator.TAB;
    
    public static DatasetReaderIterator<CheckinsDatasetBean> getCheckinsDatasetReaderIterator(File file){
        int a = 1;
        DatasetReaderIterator<CheckinsDatasetBean> dataset = new DatasetReaderIterator(file, STRING_SEPARATOR, CheckinsDatasetBean.class);
        return dataset;
    }
    
    public static DatasetReaderIterator<RatingsDatasetBean> getRatingsDatasetReaderIterator(File file){
        DatasetReaderIterator<RatingsDatasetBean> dataset = new DatasetReaderIterator(file, STRING_SEPARATOR, RatingsDatasetBean.class);
        return dataset;
    }
    
    public static DatasetReaderIterator<VenuesDatasetBean> getVenuesDatasetReaderIterator(File file){
        DatasetReaderIterator<VenuesDatasetBean> dataset = new DatasetReaderIterator(file, STRING_SEPARATOR, VenuesDatasetBean.class);
        return dataset;
    }
}
