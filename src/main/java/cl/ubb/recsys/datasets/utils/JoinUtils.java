/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.utils;

import cl.ubb.recsys.datasets.commons.DatasetReaderIterator;
import cl.ubb.recsys.datasets.commons.DatasetWriter;
import cl.ubb.recsys.datasets.commons.StringSeparator;
import cl.ubb.recsys.datasets.foursquare.umn.beans.CheckinsDatasetBean;
import cl.ubb.recsys.datasets.foursquare.umn.beans.RatedCheckinsDatasetBean;
import cl.ubb.recsys.datasets.foursquare.umn.beans.RatingsDatasetBean;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Manuel J. Vasquez
 */
public class JoinUtils {
    
    public static boolean saveRankedChekinsFromChekinsAndRatingsDataset(
                    DatasetReaderIterator<CheckinsDatasetBean> checkinsDataset,
                    DatasetReaderIterator<RatingsDatasetBean> ratingsDataset,
                    String ratedCheckinsDatasetFile){
        try{
            DatasetWriter ratedCheckinsDataset = new DatasetWriter(
                    ratedCheckinsDatasetFile, StringSeparator.TAB);
            int j = 1;
            double step = 0.01;
            double flag = step;
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            for (CheckinsDatasetBean checkingDatasetBean : checkinsDataset) {
                j++;
                double percent = j*100.0/1021967.0;
                if (percent >= flag) {
                    flag += step;
                    
                }
                for (RatingsDatasetBean ratingsDatasetBean : ratingsDataset) {
                    Integer checkingUserId = checkingDatasetBean.getUserId();
                    Integer checkingVenueId = checkingDatasetBean.getVenueId();
                    Integer ratingUserId = ratingsDatasetBean.getUserId();
                    Integer ratingVenueId = ratingsDatasetBean.getVenueId();
                    Integer rating = ratingsDatasetBean.getRating();
                    Double latitude = checkingDatasetBean.getLatitude();
                    Double longitude = checkingDatasetBean.getLongitude();
                    Date date = checkingDatasetBean.getCreatedAt();
                    if ((checkingUserId != null && checkingVenueId != null 
                            && ratingUserId != null && ratingVenueId != null
                            && rating != null && latitude != null 
                            && date != null && longitude != null) 
                            && (checkingUserId.equals(ratingUserId)
                            && checkingVenueId.equals(ratingVenueId))) {
                        RatedCheckinsDatasetBean ratedCheckinsBean;
                        ratedCheckinsBean= new RatedCheckinsDatasetBean();
                        ratedCheckinsBean.setUserId(checkingUserId);
                        ratedCheckinsBean.setVenueId(checkingVenueId);
                        ratedCheckinsBean.setRating(rating);
                        ratedCheckinsBean.setLatitude(latitude);
                        ratedCheckinsBean.setLongitude(longitude);
                        ratedCheckinsBean.setDate(date);
                        ratedCheckinsDataset.appendBean(ratedCheckinsBean);
                    }
                }
                ratingsDataset.reset();
            }
            checkinsDataset.close();
            ratingsDataset.close();
            ratedCheckinsDataset.close();
        } catch(Exception e) {
            System.out.println("Error: something goes wrong--"+e.getMessage());
            return false;
        }
        return true;
    }
    
}
