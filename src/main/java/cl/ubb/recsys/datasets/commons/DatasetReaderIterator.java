/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.commons;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

/**
 * Super class for the dataset and to work with factory pattern
 * @author Manuel J. Vasquez
 * @param <E>
 */
public class DatasetReaderIterator<E extends DatasetBean> implements Iterator<E>, Iterable<E>{
    private LineIterator data;
    private final File file;
    private final StringSeparator stringSeparator;
    private Class<E> beanClass;
    
    public DatasetReaderIterator(File file, StringSeparator separator, Class<E> beanClass){
        this.stringSeparator = separator;
        this.beanClass = beanClass;
        this.file = file;
        try {
            this.data = FileUtils.lineIterator(file, "UTF-8");
        } catch (IOException ex) {
            throw new IllegalArgumentException("The file doesn't exists or is invalid");
        }
    }
    
    public void close() {
        if (this.data != null) {
            this.data.close();
        }
    }
    
    public void reset() {
        this.close();                
        try {
            this.data = FileUtils.lineIterator(file, "UTF-8");
        } catch (IOException ex) {
            throw new IllegalArgumentException("The file doesn't exists or is invalid");
        }
    }
    
    @Override
    public boolean hasNext() {
        return this.data.hasNext();
    }

    @Override
    public E next() {
        if (this.hasNext()) {
            String line = this.data.nextLine();
            String[] rows = line.split(StringSeparator.getSeparatorAsRegex(stringSeparator),-1);
            return getBeanElement(rows);
        }
        throw new NoSuchElementException("There is no more elements in Dataset");
    }
    
    public E getBeanElement(String[] rows) {
        try {
            return beanClass.cast(beanClass.newInstance().fillFromStringRows(rows));
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new ClassCastException("Cant cast the abstract dataset to specific dataset");
        }
    }

    public StringSeparator getStringSeparator() {
        return stringSeparator;
    }

    @Override
    public Iterator<E> iterator() {
        return this;
    }

}
