/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.commons;

/**
 *
 * @author Manuel J. Vasquez
 */
public abstract class AbstractDatasetBean implements DatasetBean{
    
    @Override
    public DatasetBean fillFromStringRows(String[] rows) {
        if (rows.length != this.getIdentifiers().length) {
            throw new IllegalArgumentException("Total rows differ from bean attributes");
        }
        for (int i=0; i < rows.length; i++) {
            this.setData(this.getIdentifiers()[i], rows[i]);
        }
        return this;
    }
    
    @Override
    public String getFormattedString(StringSeparator separator){
        int size = this.getIdentifiers().length;
        String out = "";
        for (int i = 0; i < size; i++) {
            out += this.getData(this.getIdentifiers()[i]);
            if (i < size -1) {
                out += StringSeparator.geteparatorAsString(separator);
            }
        }
        return out;
    }
}
