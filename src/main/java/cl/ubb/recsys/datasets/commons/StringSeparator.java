/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.commons;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;

/**
 *
 * @author Manuel J. Vasquez
 */
public enum StringSeparator {
    TAB,
    SPACE,
    COMMA,
    SEMICOLON;
    
    public static String getSeparatorAsRegex(StringSeparator separator){
        switch(separator){
            case TAB:
                return "\\t";
            case SPACE:
                return "\\s";
            case COMMA:
                return "\\,";
            case SEMICOLON:
                return "\\;";
            default:
                return getSeparatorAsRegex(TAB);
        }
    }
    
    public static String geteparatorAsString(StringSeparator separator){
        switch(separator){
            case TAB:
                return "\t";
            case SPACE:
                return " ";
            case COMMA:
                return ",";
            case SEMICOLON:
                return ";";
            default:
                return geteparatorAsString(TAB);
        }
    }
    
}
