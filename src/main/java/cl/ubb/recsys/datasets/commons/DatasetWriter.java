/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.commons;

import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Manuel J. Vasquez
 * @param <E>
 */
public class DatasetWriter<E extends DatasetBean> {
    private PrintWriter writer;
    private StringSeparator separator;
    public DatasetWriter(String file, StringSeparator separator){
        try {
            this.writer = new PrintWriter(file, "UTF-8");
            this.separator = separator;
        } catch (IOException ex) {
            throw new IllegalArgumentException("The file "+file+" does not exists, or is inaccesible");
        }
        
    }
    public void close(){
        writer.close();
    }
    
    public void appendBean(E bean){
        this.writer.write(bean.getFormattedString(separator)+"\n");
    }
}
