/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.commons;

/**
 *
 * @author Manuel J. Vasquez
 * @param <E>
 */
public interface DatasetBean{
    public String[] getIdentifiers();
    public DatasetBean fillFromStringRows(String[] rows);
    public void setData(String identifier, String value);
    public Object getData(String identifier);
    public String getFormattedString(StringSeparator separator);
}
