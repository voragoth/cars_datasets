/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.foursquare.umn.beans;

import cl.ubb.recsys.datasets.commons.AbstractDatasetBean;

/**
 *
 * @author Manuel J. Vasquez
 */
public class VenuesDatasetBean extends AbstractDatasetBean{

    public final static String VENUE_ID = "venue_id";
    public final static String LATITUDE = "latitude";
    public final static String LONGITUDE = "longitude";
    private final static String[] IDENTIFIERS= {
        VENUE_ID,
        LATITUDE,
        LONGITUDE
    };
    
    private Integer venueId;
    private Double latitude;
    private Double longitude;
    
    @Override
    public String[] getIdentifiers() {
        return VenuesDatasetBean.IDENTIFIERS;
    }
    
    @Override
    public void setData(String identifier, String value){
        switch(identifier){
            case VENUE_ID:
                this.setVenueId(value);
                break;
            case LATITUDE:
                this.setLatitude(value);
                break;
            case LONGITUDE:
                this.setLongitude(value);
                break;
            default:
                throw new IllegalArgumentException("Identifier ["+identifier+"] does not exist in dataset");
        }
    }
    
    @Override
    public Object getData(String identifier){
              switch(identifier){
            case VENUE_ID:
                return this.getVenueId();
            case LATITUDE:
                return this.getLatitude();
            case LONGITUDE:
                return this.getLongitude();
            default:
                throw new IllegalArgumentException("Identifier ["+identifier+"] does not exist in dataset");
        }
    }
    
    public Integer getVenueId() {
        return venueId;
    }

    public void setVenueId(Integer venueId) {
        this.venueId = venueId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    private void setVenueId(String stringVenueId) {
        try {
            this.setVenueId(Integer.valueOf(stringVenueId));
        } catch(Exception e){}
    }

    private void setLatitude(String stringLatitude) {
        try{
            this.setLatitude(Double.valueOf(stringLatitude));
        } catch(Exception e){}
    }

    private void setLongitude(String stringLongitude) {
        try {
            this.setLongitude(Double.valueOf(stringLongitude));
        } catch(Exception e){}
    }
    
}
