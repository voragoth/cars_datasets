/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.foursquare.umn.beans;

import cl.ubb.recsys.datasets.commons.AbstractDatasetBean;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Manuel J. Vasquez
 */
public class CheckinsDatasetBean extends AbstractDatasetBean{

    public final static String ID = "id";
    public final static String USER_ID = "user_id";
    public final static String VENUE_ID = "venue_id";
    public final static String LATITUDE = "latitude";
    public final static String LONGITUDE = "longitude";
    public final static String CREATED_AT = "created_at";
    public final static String FORMAT_DATE = "yyyy-MM-dd HH:mm:ss";
    private final static String[] IDENTIFIERS= {
        ID,
        USER_ID,
        VENUE_ID,
        LATITUDE,
        LONGITUDE,
        CREATED_AT
    };
    
    private Integer id;
    private Integer userId;
    private Integer venueId;
    private Double latitude;
    private Double longitude;
    private Date createdAt;
    
    @Override
    public String[] getIdentifiers() {
        return CheckinsDatasetBean.IDENTIFIERS;
    }
    
    @Override
    public void setData(String identifier, String value){
        switch(identifier){
            case ID:
                this.setId(value);
                break;
            case USER_ID:
                this.setUserId(value);
                break;
            case VENUE_ID:
                this.setVenueId(value);
                break;
            case LATITUDE:
                this.setLatitude(value);
                break;
            case LONGITUDE:
                this.setLongitude(value);
                break;
            case CREATED_AT:
                this.setCreatedAt(value);
                break;
            default:
                throw new IllegalArgumentException("Identifier ["+identifier+"] does not exist in dataset");
        }
    }
    
    @Override
    public Object getData(String identifier){
              switch(identifier){
            case ID:
                return this.getId();
            case USER_ID:
                return this.getUserId();
            case VENUE_ID:
                return this.getVenueId();
            case LATITUDE:
                return this.getLatitude();
            case LONGITUDE:
                return this.getLongitude();
            case CREATED_AT:
                return this.getCreatedAt();
            default:
                throw new IllegalArgumentException("Identifier ["+identifier+"] does not exist in dataset");
        }
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getVenueId() {
        return venueId;
    }

    public void setVenueId(Integer venueId) {
        this.venueId = venueId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    
    public void setId(String stringId) {
        try{
            this.setId(Integer.valueOf(stringId));
        }catch(Exception e){}
            
    }

    private void setUserId(String stringUserId) {
        try {
            this.setUserId(Integer.valueOf(stringUserId));
        } catch(Exception e){}
    }

    private void setVenueId(String stringVenueId) {
        try {
            this.setVenueId(Integer.valueOf(stringVenueId));
        } catch(Exception e){}
    }

    private void setLatitude(String stringLatitude) {
        try{
            this.setLatitude(Double.valueOf(stringLatitude));
        } catch(Exception e){}
    }

    private void setLongitude(String stringLongitude) {
        try {
            this.setLongitude(Double.valueOf(stringLongitude));
        } catch(Exception e){}
    }

    private void setCreatedAt(String stringCreatedAt) {
        if ("".trim().equals(stringCreatedAt)) {
            return;
        }
        try {
            SimpleDateFormat formater = new SimpleDateFormat(FORMAT_DATE);
            this.setCreatedAt(formater.parse(stringCreatedAt));
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }
    
}
