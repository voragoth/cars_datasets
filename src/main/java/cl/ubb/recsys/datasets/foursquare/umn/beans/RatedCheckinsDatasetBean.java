/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.foursquare.umn.beans;

import cl.ubb.recsys.datasets.commons.AbstractDatasetBean;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Manuel J. Vasquez
 */
public class RatedCheckinsDatasetBean extends AbstractDatasetBean{
    public final static String USER_ID = "user_id";
    public final static String VENUE_ID = "venue_id";
    public final static String LATITUDE = "latitude";
    public final static String LONGITUDE = "longitude";
    public final static String DATE = "date";
    public final static String RATING = "rating";
    
    public final static String FORMAT_DATE = "yyyy-MM-dd HH:mm:ss";
    private final static String[] IDENTIFIERS= {
        USER_ID,
        VENUE_ID,
        LATITUDE,
        LONGITUDE,
        DATE,
        RATING
    };
    
    private Integer userId;
    private Integer venueId;
    private Double latitude;
    private Double longitude;
    private Date date;
    private Integer rating;
    
    @Override
    public String[] getIdentifiers() {
        return RatedCheckinsDatasetBean.IDENTIFIERS;
    }
    
    @Override
    public void setData(String identifier, String value){
        switch(identifier){
            case USER_ID:
                this.setUserId(value);
                break;
            case VENUE_ID:
                this.setVenueId(value);
                break;
            case LATITUDE:
                this.setLatitude(value);
                break;
            case LONGITUDE:
                this.setLongitude(value);
                break;
            case DATE:
                this.setDate(value);
                break;
            case RATING:
                this.setDate(value);
                break;
            default:
                throw new IllegalArgumentException("Identifier ["+identifier+"] does not exist in dataset");
        }
    }
    
    @Override
    public Object getData(String identifier){
        switch(identifier){
            case USER_ID:
                return this.getUserId();
            case VENUE_ID:
                return this.getVenueId();
            case LATITUDE:
                return this.getLatitude();
            case LONGITUDE:
                return this.getLongitude();
            case DATE:
                return this.getDate();
            case RATING:
                return null;
            default:
                throw new IllegalArgumentException("Identifier ["+identifier+"] does not exist in dataset");
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getVenueId() {
        return venueId;
    }

    public void setVenueId(Integer venueId) {
        this.venueId = venueId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
    
    public void setRating(String stringRating) {
        try{
            this.setRating(Integer.valueOf(stringRating));
        }catch(Exception e){}
            
    }

    private void setUserId(String stringUserId) {
        try {
            this.setUserId(Integer.valueOf(stringUserId));
        } catch(Exception e){}
    }

    private void setVenueId(String stringVenueId) {
        try {
            this.setVenueId(Integer.valueOf(stringVenueId));
        } catch(Exception e){}
    }

    private void setLatitude(String stringLatitude) {
        try{
            this.setLatitude(Double.valueOf(stringLatitude));
        } catch(Exception e){}
    }

    private void setLongitude(String stringLongitude) {
        try {
            this.setLongitude(Double.valueOf(stringLongitude));
        } catch(Exception e){}
    }

    private void setDate(String stringDate) {
        if ("".trim().equals(stringDate)) {
            return;
        }
        try {
            SimpleDateFormat formater = new SimpleDateFormat(FORMAT_DATE);
            this.setDate(formater.parse(stringDate));
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }
    
}
