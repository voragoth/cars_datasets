/*
 * Project made exclusively for master's thesis in computer science
 * by Manuel J. Vasquez Cruz
 * 2015-2016 (c)
 */
package cl.ubb.recsys.datasets.foursquare.umn.beans;

import cl.ubb.recsys.datasets.commons.AbstractDatasetBean;

/**
 *
 * @author Manuel J. Vasquez
 */
public class RatingsDatasetBean extends AbstractDatasetBean{
    
    public final static String USER_ID = "user_id";
    public final static String VENUE_ID = "venue_id";
    public final static String RATING = "rating";
    private final static String[] IDENTIFIERS= {
        USER_ID,
        VENUE_ID,
        RATING
    };
    
    private Integer userId;
    private Integer venueId;
    private Integer rating;
    
    @Override
    public String[] getIdentifiers() {
        return RatingsDatasetBean.IDENTIFIERS;
    }
    
    @Override
    public void setData(String identifier, String value){
        switch(identifier){
            case USER_ID:
                this.setUserId(value);
                break;
            case VENUE_ID:
                this.setVenueId(value);
                break;
            case RATING:
                this.setRating(value);
                break;
            default:
                throw new IllegalArgumentException("Identifier ["+identifier+"] does not exist in dataset");
        }
    }
    
    @Override
    public Object getData(String identifier){
        switch(identifier){
            case USER_ID:
                return this.getUserId();
            case VENUE_ID:
                return this.getVenueId();
            case RATING:
                return this.getRating();
            default:
                throw new IllegalArgumentException("Identifier ["+identifier+"] does not exist in dataset");
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getVenueId() {
        return venueId;
    }

    public void setVenueId(Integer venueId) {
        this.venueId = venueId;
    }

    public Integer getRating() {
        return this.rating;
    }
    
    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public void setUserId(String stringUserId) {
        try {
            this.setUserId(Integer.valueOf(stringUserId));
        } catch (Exception e){}
    }

    public void setVenueId(String stringVenueId) {
        try {
            this.setVenueId(Integer.valueOf(stringVenueId));
        } catch (Exception e){}
    }

    public void setRating(String stringRating) {
        try {
            this.setRating(Integer.valueOf(stringRating));
        } catch (Exception e){}
    }

}
